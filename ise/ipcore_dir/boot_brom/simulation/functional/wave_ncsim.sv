

 
 
 




window new WaveWindow  -name  "Waves for BMG Example Design"
waveform  using  "Waves for BMG Example Design"

      waveform add -signals /boot_brom_tb/status
      waveform add -signals /boot_brom_tb/boot_brom_synth_inst/bmg_port/CLKA
      waveform add -signals /boot_brom_tb/boot_brom_synth_inst/bmg_port/ADDRA
      waveform add -signals /boot_brom_tb/boot_brom_synth_inst/bmg_port/DOUTA

console submit -using simulator -wait no "run"
