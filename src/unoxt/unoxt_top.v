`timescale 1ns / 1ns
`default_nettype wire

//    This file is part of the UnoXT Dev Board Project. 
//    (copyleft)2022 emax73.
//    UnoXT official repository: https://gitlab.com/emax73g/unoxt-hardware
//
//    UnoXT core is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    UnoXT core is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with the UnoXT core.  If not, see <https://www.gnu.org/licenses/>.
//
//    Any distributed copy of this file must keep this notice intact.

//`define CORE_25MHZ 

//Max
//UnoXT Generation Selector
`define unoxt
//`define unoxt2

`ifdef unoxt
	module unoxt_top (
`elsif unoxt2
	module unoxt2_top (
`else
	module unoxt_top (
`endif

   input wire clock_50_i,
	
   output wire [4:0] rgb_r_o,
   output wire [4:0] rgb_g_o,
   output wire [4:0] rgb_b_o,
   output wire hsync_o,
   output wire vsync_o,
   input wire ear_port_i,
   output wire mic_port_o,
   inout wire ps2_clk_io,
   inout wire ps2_data_io,
   inout wire ps2_pin6_io,
   inout wire ps2_pin2_io,
   output wire audioext_l_o,
   output wire audioext_r_o,

   inout wire esp_gpio0_io,
   inout wire esp_gpio2_io,
   output wire esp_tx_o,
   input wire esp_rx_i,
 
   output wire [20:0] ram_addr_o,
   output wire ram_lb_n_o,
   output wire ram_ub_n_o,
   inout wire [15:0] ram_data_io,
   output wire ram_oe_n_o,
   output wire ram_we_n_o,
   output wire ram_ce_n_o,
   
   output wire flash_cs_n_o,
   output wire flash_sclk_o,
   output wire flash_mosi_o,
   input wire flash_miso_i,
   output wire flash_wp_o,
   output wire flash_hold_o,
   
   input wire joyp1_i,
   input wire joyp2_i,
   input wire joyp3_i,
   input wire joyp4_i,
   input wire joyp6_i,
   output wire joyp7_o,
   input wire joyp9_i,
	
   input wire btn_divmmc_n_i,
   input wire btn_multiface_n_i,

   output wire sd_cs0_n_o,    
   output wire sd_sclk_o,     
   output wire sd_mosi_o,    
   input wire sd_miso_i,

   output wire led_red_o,
   output wire led_yellow_o,
   output wire led_green_o,
   output wire led_blue_o,

	//Max
   output wire [8:0] test

   );

	wire rgb_r, rgb_g, rgb_b;
	
	wire [7:0] greenleds;
   wire [7:0] switch;
	assign switch = 8'b10000001;
	
	wire tape_out;
	wire pll_locked;
	
	reg hypercharge;
	wire reset;
	assign reset = !btn_divmmc_n_i || !pll_locked;
	
   wire cpu_rd;
   wire cpu_wt;
   wire cpu_oe_n;
   wire ifetch;
   wire [17:0] cpu_adr;
   wire [7:0] redleds;
   wire [15:0] cpu_opcode;
   wire [15:0] cpu_sp;
   wire [15:0] ram_out_data;
	wire [15:0] joy1;

	bk0010 elektronika(
	   .clk50(clk50),
		.clk25(clk25),
		.reset_in(reset),
		.hypercharge_i(hypercharge),
		.PS2_Clk(ps2_clk_io),
		.PS2_Data(ps2_data_io),
		.button0(1'b0),
		
		.iTCK(1'b1),
		.oTDO(),
		.iTDI(1'b1),
		.iTCS(1'b1),
		
		.SD_DAT(sd_miso_i),
		.SD_DAT3(sd_cs0_n_o),
		.SD_CMD(sd_mosi_o),
		.SD_CLK(sd_sclk_o),

		.greenleds(greenleds),
		.switch(switch),
		
		.ram_addr(ram_addr_o[17:0]),
		.ram_a_data(ram_data_io),
		.ram_a_ce(ram_ce_n_o),
		.ram_a_lb(ram_lb_n_o),
		.ram_a_ub(ram_ub_n_o),
		.ram_we_n(ram_we_n_o),
		.ram_oe_n(ram_oe_n_o),
		
		.VGA_RED(rgb_r),
		.VGA_GREEN(rgb_g),
		.VGA_BLUE(rgb_b),
		.VGA_VS(vsync_o),
		.VGA_HS(hsync_o),
		
		.tape_out(tape_out),
		.tape_in(ear_port_i),
		
		.cpu_rd(cpu_rd),
		.cpu_wt(cpu_wt),
		.cpu_oe_n(cpu_oe_n),
		.ifetch(ifetch),
		.cpu_adr(cpu_adr),
		.redleds(redleds),
		.cpu_opcode(cpu_opcode),
		.cpu_sp(cpu_sp),
		.ram_out_data(ram_out_data),
		
		//Max
		.joy1(joy1),
		.test(test)
);

	pll pllI (
		.CLK_IN1(clock_50_i),
		.CLK_OUT1(clk50),
		.CLK_OUT2(clk25),
		.RESET(1'b0),
		.LOCKED(pll_locked)
	);

	assign audioext_l_o = tape_out;
	assign audioext_r_o = tape_out;
	assign mic_port_o = tape_out;

	assign rgb_r_o = { rgb_r, rgb_r, rgb_r, rgb_r, rgb_r };
	assign rgb_g_o = { rgb_g, rgb_g, rgb_g, rgb_g, rgb_g };
	assign rgb_b_o = { rgb_b, rgb_b, rgb_b, rgb_b, rgb_b };

 	//assign joyp7_o = 1'b1;
	//assign joy1 = {joyp6_i, joyp1_i, joyp2_i, joyp3_i, joyp4_i};
	
	assign ram_addr_o[20:18] = 3'b000;
	
	assign esp_gpio0_io = 1'b1;
	assign esp_gpio2_io = 1'b1;
	assign esp_tx_o = 1'b1;
	
	assign flash_cs_n_o = 1'b1;
	assign flash_sclk_o = 1'b1;
	assign flash_mosi_o = 1'b1;
	assign flash_wp_o = 1'b1;
	assign flash_hold_o = 1'b1;
	
reg [15:0] debctr;
reg        debsamp;
always @(posedge clk25 or posedge reset) begin
    if (reset) begin
        debsamp <= 0;
        debctr <= 0;
    end else begin
        if (!btn_multiface_n_i)    
            debctr <= 16'hffff;
        else
            if (|debctr)    debctr <= debctr - 1;
        
        debsamp <= |debctr;
        
        if (~debsamp && |debctr)    hypercharge <= ~hypercharge;
    end
end
	
	wire greenLed;
	//assign greenLed = !sd_cs0_n_o;
	flashCnt #(.CLK_MHZ(16'd50)) cnt(
		.clk(clk50),
		.signal(sd_sclk_o),
		.msec(16'd20),
		.flash(greenLed)
	);
	
	/*assign turbo100 = (turbo == 2'b00) ? 16'd0 :
							(turbo == 2'b01) ? 16'd33 :
							(turbo == 2'b10) ? 16'd66 :
							(turbo == 2'b11) ? 16'd100 :
							16'd0;
	*/
	
  wire [11:0] joyA;
	
  joystickMD #(.CLK_MHZ(16'd50)) joyMD 
  (
 		.clk(clk50),
		.joyp1_i(joyp1_i),
		.joyp2_i(joyp2_i),
		.joyp3_i(joyp3_i),
		.joyp4_i(joyp4_i),
		.joyp6_i(joyp6_i),
		.joyp7_o(joyp7_o),
		.joyp9_i(joyp9_i),
		.joyOut(joyA)		// MXYZ SACB UDLR  1- On 0 - off
		//.test(test)
  );
  
  //10 	- UP
  //5		- DOWN
  //9		- LEFT
  //4		- RIGHT
  //0		- FIRE 1: (KP /)
  //1		- FIRE 2: (KP 0)
  //2		- FIRE 3: (KP 5)
  //3		- FIRE 4: (KP ENTER)
  
  assign joy1 = { 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, joyA[3], joyA[1], 1'b0, 1'b0, 1'b0, joyA[2], joyA[0], joyA[10], joyA[5], joyA[4], joyA[6] };

	assign led_red_o = 1'b1;
	assign led_yellow_o = hypercharge;
	assign led_green_o = greenLed;
	assign led_blue_o = 1'b0;

 	/*ledPWM ledRed(
		.nReset(1'b1),
		.clock(clk50),
		.enable(1'b1),
		.y1(16'd100),
		.y2(ledRedK),	
		.led(led_red_o)
    );

	ledPWM ledYellow(
		.nReset(1'b1),
		.clock(clk50),
		.enable(1'b1),
		.y1(turbo100),
		.y2(ledYellowK),	
		.led(led_yellow_o)
    );

	ledPWM ledGreen(
		.nReset(1'b1),
		.clock(clk50),
		.enable(greenLed),
		.y1(16'd100),
		.y2(ledGreenK),	
		.led(led_green_o)
    );

	ledPWM ledBlue(
		.nReset(1'b1),
		.clock(clk50),
		.enable(blueLed),
		.y1(16'd100),
		.y2(ledBlueK),	
		.led(led_blue_o)
    );*/
	 
	 /*assign test[0] = sd_cs0_n_o;
	 assign test[1] = sd_sclk_o;
	 assign test[2] = sd_mosi_o;
	 assign test[3] = sd_miso_i;
	 assign test[4] = 1'b1;
	 assign test[5] = test5;
	 assign test[6] = 1'b1;
	 assign test[7] = 1'b1;
	 assign test[8] = 1'b1;
	 */
  
endmodule
