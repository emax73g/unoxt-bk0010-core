namespace mif2coe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btConvert_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines("boot.mif");
            string[] tokens;
            string data;
            StreamWriter stream = new StreamWriter("boot.coe.data");
            int j = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                tokens = lines[i].Split(':');
                if (tokens.Length == 2)
                {
                    data = tokens[1].Replace(';', ' ').Trim();
                    stream.Write(data + ", ");
                    j++;
                    if ((j % 16) == 0)
                    {
                        stream.Write("\r\n");
                    }
                }
                stream.Flush();
            }
            stream.Close();
            MessageBox.Show("Completed!");
        }
    }
}